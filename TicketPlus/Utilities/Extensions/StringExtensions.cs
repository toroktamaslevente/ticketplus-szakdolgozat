﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketPlus.Utilities.Extensions
{
    public static class StringExtensions
    {
        public static bool AsBool(this string input)
        {
            string s = input.ToLower();
            return s == "1" || s == "true" || s == "yes";
        }

        public static ushort AsUnsignedShort(this string input)
        {
            return ushort.TryParse(input, out ushort result) ? result : (ushort)0;
        }
    }
}
