﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketPlus.Utilities
{
    public class Keys
    {
        public const string Containerized = "CONTAINERIZED";

        public const string MySQLHost = "MYSQL_HOST";
        public const string MySQLPort = "MYSQL_PORT";
        public const string MySQLUsername = "MYSQL_USER";
        public const string MySQLPassword = "MYSQL_PASSWORD";
        public const string MySQLDatabaseName = "MYSQL_DATABASE";

        public const string MySQLConnectionStringName = "MySQL";
    }
}
