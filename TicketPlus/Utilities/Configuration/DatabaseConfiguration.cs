﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketPlus.Utilities.Extensions;

namespace TicketPlus.Utilities.Configuration
{
    public class DatabaseConfiguration
    {
        private string host = "db-mysql-fra1-do-user-2855192-0.b.db.ondigitalocean.com";
        private ushort port = 25060;
        private string username = "TicketPlus";
        private string password = "m593lx1qAzmaZpLI";
        private string databaseName = "TicketPlus";

        public string Host { get => host; set => host = value; }
        public ushort Port { get => port; set => port = value; }
        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public string DatabaseName { get => databaseName; set => databaseName = value; }
        public string ConnectionString => $"Server={Host};Port={Port};Database={DatabaseName};Uid={Username};Pwd={Password};";

        public void LoadFromEnvironment()
        {
            Host = Environment.GetEnvironmentVariable(Keys.MySQLHost) ?? host;
            Port = (Environment.GetEnvironmentVariable(Keys.MySQLPort) ?? port.ToString()).AsUnsignedShort();
            Username = Environment.GetEnvironmentVariable(Keys.MySQLUsername) ?? username;
            Password = Environment.GetEnvironmentVariable(Keys.MySQLPassword) ?? password;
            DatabaseName = Environment.GetEnvironmentVariable(Keys.MySQLDatabaseName) ?? databaseName;
        }

    }
}
